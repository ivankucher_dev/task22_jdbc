package com.epam.trainings.db.service;

import com.epam.trainings.db.dao.GeneralDao;
import com.epam.trainings.mvc.model.Car;

public interface CarService extends GeneralDao<Car> {
    boolean deleteByCarNumber(long number);
}
