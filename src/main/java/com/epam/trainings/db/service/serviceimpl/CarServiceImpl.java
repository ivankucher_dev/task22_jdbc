package com.epam.trainings.db.service.serviceimpl;

import com.epam.trainings.db.dao.CarDao;
import com.epam.trainings.db.dao.daoimpl.CarDaoImpl;
import com.epam.trainings.db.service.CarService;
import com.epam.trainings.mvc.model.Car;
import com.epam.trainings.mvc.model.User;

import java.util.List;

public class CarServiceImpl implements CarService {

    private CarDao<Car> carDao;

    public CarServiceImpl(CarDao<Car> carDao){
        this.carDao = carDao;
    }

    @Override
    public boolean deleteByCarNumber(long number) {
        return carDao.deleteByCarNumber(number);
    }

    @Override
    public Car findById(int id) {
        return carDao.findById(id);
    }

    @Override
    public List<Car> findAll() {
        return carDao.findAll();
    }

    @Override
    public boolean create(Car car) {
        return carDao.create(car);
    }

    @Override
    public boolean update(Car car) {
        return carDao.update(car);
    }

    @Override
    public boolean delete(Car car) {
        return carDao.delete(car);
    }
}
