package com.epam.trainings.db.service.serviceimpl;

import com.epam.trainings.db.dao.CarDao;
import com.epam.trainings.db.dao.EmailDao;
import com.epam.trainings.db.service.EmailService;
import com.epam.trainings.mvc.model.Car;
import com.epam.trainings.mvc.model.Email;

import java.util.List;

public class EmailServiceImpl implements EmailService {

    private EmailDao<Email> emailDao;

    public EmailServiceImpl(EmailDao<Email> emailDao){
      this.emailDao = emailDao;
    }

    @Override
    public Email findById(int id) {
        return emailDao.findById(id);
    }

    @Override
    public List<Email> findAll() {
        return emailDao.findAll();
    }

    @Override
    public boolean create(Email email) {
        return emailDao.create(email);
    }

    @Override
    public boolean update(Email email) {
        return emailDao.update(email);
    }

    @Override
    public boolean delete(Email email) {
        return emailDao.delete(email);
    }
}
