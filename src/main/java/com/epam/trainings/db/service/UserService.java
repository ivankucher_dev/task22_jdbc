package com.epam.trainings.db.service;

import com.epam.trainings.db.dao.GeneralDao;
import com.epam.trainings.mvc.model.User;

public interface UserService extends GeneralDao<User> {
    boolean changeUserPass(User entity);
}
