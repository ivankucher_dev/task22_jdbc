package com.epam.trainings.db.service;

import com.epam.trainings.db.dao.GeneralDao;
import com.epam.trainings.mvc.model.Email;

public interface EmailService extends GeneralDao<Email> {
}
