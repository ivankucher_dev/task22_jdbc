package com.epam.trainings.db.service.serviceimpl;

import com.epam.trainings.db.dao.UserDao;
import com.epam.trainings.db.service.UserService;
import com.epam.trainings.mvc.model.User;

import java.util.List;

public class UserServiceImpl implements UserService {

    private UserDao<User> userDao;

    public UserServiceImpl(UserDao<User> userDao) {
        this.userDao = userDao;
    }

    @Override
    public boolean changeUserPass(User user) {
        return userDao.changeUserPass(user);
    }

    @Override
    public User findById(int id) {
        return userDao.findById(id);
    }

    @Override
    public List<User> findAll() {
        return userDao.findAll();
    }

    @Override
    public boolean create(User user) {
        return userDao.create(user);
    }

    @Override
    public boolean update(User user) {
        return userDao.update(user);
    }

    @Override
    public boolean delete(User user) {
        return userDao.delete(user);
    }
}
