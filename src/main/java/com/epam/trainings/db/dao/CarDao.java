package com.epam.trainings.db.dao;

public interface CarDao<T> extends GeneralDao<T> {
    boolean deleteByCarNumber(long number);
}
