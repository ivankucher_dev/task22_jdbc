package com.epam.trainings.db.dao;

import java.util.List;

public interface GeneralDao<T> {
    T findById(int id);
    List<T> findAll();
    boolean create(T entity);
    boolean update(T entity);
    boolean delete(T entity);

}
