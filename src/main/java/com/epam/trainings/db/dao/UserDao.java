package com.epam.trainings.db.dao;

public interface UserDao<T> extends GeneralDao<T> {
    boolean changeUserPass(T entity);
}
