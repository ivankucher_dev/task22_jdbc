package com.epam.trainings.db.dao.daoimpl;

import com.epam.trainings.db.dao.EmailDao;
import com.epam.trainings.mvc.model.Email;

import java.util.List;

public class EmailDaoImpl implements EmailDao<Email> {
    @Override
    public Email findById(int id) {
        return null;
    }

    @Override
    public List<Email> findAll() {
        return null;
    }

    @Override
    public boolean create(Email entity) {
        return false;
    }

    @Override
    public boolean update(Email entity) {
        return false;
    }

    @Override
    public boolean delete(Email entity) {
        return false;
    }
}
