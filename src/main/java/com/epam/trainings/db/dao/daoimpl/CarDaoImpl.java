package com.epam.trainings.db.dao.daoimpl;

import com.epam.trainings.db.DBConnection;
import com.epam.trainings.db.dao.CarDao;
import com.epam.trainings.mvc.model.Car;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CarDaoImpl implements CarDao<Car> {

    private Connection connection=null;
    private PreparedStatement ptmt = null;
    private ResultSet resultSet = null;


    private Connection getConnection(){
        return DBConnection.getDbConnection();
    }

    @Override
    public boolean deleteByCarNumber(long number) {
        return false;
    }

    @Override
    public Car findById(int id) {
        return null;
    }

    @Override
    public List<Car> findAll() {
        List<Car> list = new ArrayList<>();
        try {
            String queryString = "SELECT * FROM cars";
            connection = getConnection();
            ptmt = connection.prepareStatement(queryString);
            resultSet = ptmt.executeQuery();
            while (resultSet.next()) {
               Car car = new Car();
               car.setId(resultSet.getInt("id"));
               car.setName(resultSet.getString("car_name"));
               car.setNumber(resultSet.getInt("number"));
               list.add(car);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (resultSet != null)
                    resultSet.close();
                if (ptmt != null)
                    ptmt.close();
                if (connection != null)
                    connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        return list;
    }

    @Override
    public boolean create(Car entity) {
        try {
            connection = getConnection();
            String queryString = "INSERT INTO cars(car_name, number) VALUES(?,?)";
            ptmt = connection.prepareStatement(queryString);
            ptmt.setString(1, entity.getName());
            ptmt.setInt(2, entity.getNumber());
            ptmt.executeUpdate();
            System.out.println("Data Added Successfully");
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (ptmt != null)
                    ptmt.close();
                if (connection != null)
                    connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        return false;
    }

    @Override
    public boolean update(Car entity) {
        try {
            connection = getConnection();
            String queryString = "UPDATE cars SET car_name=? , number=?";
            ptmt = connection.prepareStatement(queryString);
            ptmt.setString(1, entity.getName());
            ptmt.setInt(2, entity.getNumber());
            ptmt.executeUpdate();
            System.out.println("Table Updated Successfully");
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (ptmt != null)
                    ptmt.close();
                if (connection != null)
                    connection.close();
            }

            catch (SQLException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();

            }
        }
        return true;
    }

    @Override
    public boolean delete(Car entity) {
        try {
            connection = getConnection();
            String queryString = "DELETE FROM cars WHERE id=?";
            ptmt = connection.prepareStatement(queryString);
            ptmt.setInt(1, entity.getId());
            ptmt.executeUpdate();
            System.out.println("Data deleted Successfully");
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (ptmt != null)
                    ptmt.close();
                if (connection != null)
                    connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        return false;
    }
}
