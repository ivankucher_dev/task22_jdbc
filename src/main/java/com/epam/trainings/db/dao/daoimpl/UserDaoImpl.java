package com.epam.trainings.db.dao.daoimpl;

import com.epam.trainings.db.dao.UserDao;
import com.epam.trainings.mvc.model.User;

import java.util.List;

public class UserDaoImpl implements UserDao<User> {
    @Override
    public boolean changeUserPass(User entity) {
        return false;
    }

    @Override
    public User findById(int id) {
        return null;
    }

    @Override
    public List<User> findAll() {
        return null;
    }

    @Override
    public boolean create(User entity) {
        return false;
    }

    @Override
    public boolean update(User entity) {
        return false;
    }

    @Override
    public boolean delete(User entity) {
        return false;
    }
}
