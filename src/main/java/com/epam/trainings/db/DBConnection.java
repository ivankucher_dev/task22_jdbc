package com.epam.trainings.db;

import com.epam.trainings.utils.PropertiesGetter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class DBConnection {
  private static java.sql.Connection dbConnection;
  private static Properties properties;
  private static String dbUser;
  private static String dbPass;
  private static String connectionString;
  private static Logger log = LogManager.getLogger(DBConnection.class.getName());

  public static java.sql.Connection getDbConnection() {
    if (dbConnection == null) {
      configFields();
      try {
        Class.forName("com.mysql.cj.jdbc.Driver");
        try {
          dbConnection = DriverManager.getConnection(connectionString, dbUser, dbPass);
        } catch (SQLException e) {
          log.error("Failed to create db connection");
          e.printStackTrace();
        }
      } catch (ClassNotFoundException e) {
        log.error("Driver not found");
      }
      return dbConnection;
    }
    return dbConnection;
  }

  private static void configFields() {
    properties = PropertiesGetter.getPropertiesFile("connection.properties");
    dbUser = properties.getProperty("dbUser");
    dbPass = properties.getProperty("dbPass");
    connectionString = properties.getProperty("connection");
  }
}
