package com.epam.trainings.annotations.annotationhandlers;

import com.epam.trainings.annotations.tableannotations.Column;

import java.lang.reflect.Field;

public class ColumnHandler {

  public static String handleColumnField(Field field) {
    Column column = (Column) field.getAnnotation(Column.class);
    String columnName = column.name();
    if (columnName.isEmpty()) {
      columnName = field.getName();
    }
    return columnName;
  }
}
