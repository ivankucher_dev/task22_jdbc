package com.epam.trainings.exceptions;

public class TableCreationException extends RuntimeException {
  public TableCreationException(String message) {
    super(message);
  }
}
