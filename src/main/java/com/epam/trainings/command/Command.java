package com.epam.trainings.command;

public interface Command {
  String execute();
}
