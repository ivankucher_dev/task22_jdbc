package com.epam.trainings.mvc.model.menu;

import java.util.List;

public interface SimpleMainMenu {
  String show();

  String greeting();

  void setGreeting(String greeting);

  void setLanguageChoose(String languageChoose);

  void setNameForMenuTask(List<String> menuText);
}
