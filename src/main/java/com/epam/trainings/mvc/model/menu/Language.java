package com.epam.trainings.mvc.model.menu;

import com.epam.trainings.utils.PropertiesGetter;

import java.util.*;

public class Language {
  private HashMap<Integer, String> avaliableLanguages;
  private List<String> menuText;
  private SimpleMainMenu menu;
  private ResourceBundle bundle;
  private Properties properties;

  public Language(SimpleMainMenu menu) {
    this.menu = menu;
    properties = PropertiesGetter.getProperties();
    avaliableLanguages = new HashMap<>();
    fillLanguagesMap();
    updateLanguage(new Locale(avaliableLanguages.get(1)));
    PresentLocale.setLocale(new Locale(avaliableLanguages.get(1)));
  }

  public HashMap<Integer, String> getAvaliableLanguages() {
    return avaliableLanguages;
  }

  private void fillLanguagesMap() {
    avaliableLanguages.put(1, properties.getProperty("english_menu"));
    avaliableLanguages.put(2, properties.getProperty("ukrainian_menu"));
    avaliableLanguages.put(3, properties.getProperty("chinese_menu"));
  }

  public void showLanguageList() {
    avaliableLanguages
        .entrySet()
        .forEach(
            entry -> {
              System.out.println(entry.getKey() + " - " + entry.getValue());
            });
  }

  public void updateLanguage(Locale locale) {
    menuText = new ArrayList<>();
    PresentLocale.setLocale(locale);
    updateMenuBasedOnLanguage(locale);
  }

  private void updateMenuBasedOnLanguage(Locale locale) {
    bundle = ResourceBundle.getBundle("MyMenu", locale);
    menu.setGreeting(bundle.getString("greeting"));
    menu.setLanguageChoose(bundle.getString("languageChoose"));
    menuText.add(bundle.getString("menu_item1"));
    menu.setNameForMenuTask(menuText);
  }
}
