package com.epam.trainings.mvc.model;

import com.epam.trainings.annotations.tableannotations.Column;
import com.epam.trainings.annotations.tableannotations.Entity;
import com.epam.trainings.annotations.tableannotations.Id;

@Entity
public class Car {

  @Id private int id;

  @Column(name = "car_name")
  private String name;

  private int number;

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getNumber() {
    return number;
  }

  public void setNumber(int number) {
    this.number = number;
  }
}
