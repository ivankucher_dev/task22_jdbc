package com.epam.trainings.mvc.model.menu;

import java.util.ArrayList;
import java.util.List;

public class Menu implements SimpleMainMenu {
    private String greeting;
    private String languageChoose;
    private List<String> menu_tasks;

  public void setGreeting(String greeting) {
    this.greeting = greeting;
  }

  public void setLanguageChoose(String languageChoose) {
    this.languageChoose = languageChoose;
  }

  public void setNameForMenuTask(List<String> menuText) {
        menu_tasks = new ArrayList<>();
        menu_tasks = menuText;
    }

    @Override
    public String show() {
        StringBuilder sb = new StringBuilder();
        sb.append(languageChoose);
        menu_tasks.forEach(e -> sb.append("\n" + e));
        return sb.toString();
    }

    @Override
    public String greeting() {
        StringBuilder sb = new StringBuilder();
        sb.append(greeting);
        return sb.toString();
    }
}
