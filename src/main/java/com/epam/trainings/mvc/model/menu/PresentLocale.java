package com.epam.trainings.mvc.model.menu;

import java.util.Locale;

public class PresentLocale {
  public static Locale locale;

  public static void setLocale(Locale locale) {
    PresentLocale.locale = locale;
  }
}
