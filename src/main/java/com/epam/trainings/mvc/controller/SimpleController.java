package com.epam.trainings.mvc.controller;

import com.epam.trainings.command.ChangeLanguageCommand;
import com.epam.trainings.mvc.model.table.Table;

import java.util.List;

public interface SimpleController {
  List<Table> getTables();

  Table getTable(Object object);

  String showMenu();

  String execute(int command);

  String showTable();
}
